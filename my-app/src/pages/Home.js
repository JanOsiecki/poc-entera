import Login from "../components/Login";
import Users from "../components/Users";
import Logout from "../components/Logout";


function Home() {
  return (
    <div className="App">
      <Login />
      <Users/>
      <Logout/>
    </div>
  );
}
export default Home;
