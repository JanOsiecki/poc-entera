import React, { useEffect, useState } from 'react';
// import { useHistory } from 'react-router-dom';
import { useNavigate } from "react-router-dom";

function AuthRedirect() {
    const [error, setError] = useState('');
    const [login, setLogin] = useState(false);
    let navigate = useNavigate();

    // const history = useHistory();

    useEffect(() => {
        const urlParams = new URLSearchParams(window.location.search);
        const code = urlParams.get('code');

        if (code) {
            fetch('http://localhost:8000/server/auth/microsoft/', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                credentials: 'include',
                body: JSON.stringify({ code }),
            })
            .then(response => {
                if (response.ok) {
                    console.log("AUTH")
                    return navigate("/");
                } else {
                    throw new Error('Failed to authenticate');
                }
            })
            .catch(error => {
                setError(error.message);
            });
        }
    }, []);
    return (
        <div>
            {error && <p>Error: {error}</p>}
        </div>
    );
}

export default AuthRedirect;

