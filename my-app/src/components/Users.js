import React, { useState } from 'react';

function UsersComponent() {
    const [users, setUsers] = useState(null);
    const [error, setError] = useState('');

    const fetchUsers = async () => {
        try {
            const response = await fetch('http://localhost:8000/users/',{
                credentials: 'include',

            });
            if (response.status === 401) {
                throw new Error('Authentication error');
            }
            const data = await response.json();
            setUsers(data);
            setError('');
        } catch (err) {
            setUsers(null);
            setError(err.message);
        }
    };

    return (
        <div>
            <button onClick={fetchUsers}>Load Users</button>
            {error && <p>{error}</p>}
            {users && (
                <div>
                    <h2>Users:</h2>
                    <ul>
                        {users.map(user => (
                            <li key={user.username}>{user.username}</li>
                        ))}
                    </ul>
                </div>
            )}
        </div>
    );
}

export default UsersComponent;