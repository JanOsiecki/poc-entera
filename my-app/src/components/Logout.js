import React, { useState } from 'react';
export default function Login() {
 
    
    const logOut = async () => {
        try {
            const response = await fetch('http://localhost:8000/logout/',{
                credentials: 'include', // if needed to handle cookies
            headers: {
                'Content-Type': 'application/json',
            },
            });
            if (response.status === 401) {
                throw new Error('Authentication error');
            }
            const data = await response.json();

        } catch (err) {

        }
    };
  return (
    <div>
      <div>
      <button onClick={logOut}>LogOut</button>
      </div>
    </div>
  );
}