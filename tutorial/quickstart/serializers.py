from django.contrib.auth.models import Group, User
from rest_framework import serializers
from dj_rest_auth.serializers import JWTSerializer
from rest_framework_simplejwt.serializers import TokenRefreshSerializer

class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ['url', 'username', 'email', 'groups']


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ['url', 'name']


from rest_framework_simplejwt.serializers import TokenObtainPairSerializer


class MyTokenObtainPairSerializer(TokenObtainPairSerializer):
    @classmethod
    def get_token(cls, user):
        token = super().get_token(user)
        print(user.groups)
        # Add custom claims
        token['hello'] = "test123"
        # ...

        return token


class SafeJWTSerializer(JWTSerializer):
    def __init__(self, *args, **kwargs):
        super(JWTSerializer, self).__init__(*args, **kwargs)
        self.fields.pop("access")
        self.fields.pop("refresh")
        self.fields.pop("user")


class SafeRefreshSerializer(TokenRefreshSerializer):
    def __init__(self, *args, **kwargs):
        super(TokenRefreshSerializer, self).__init__(*args, **kwargs)
        self.fields.pop("access")


from rest_framework import serializers


class EmptySerializer(serializers.Serializer):
    pass
