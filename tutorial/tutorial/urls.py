import urllib.parse

from allauth.socialaccount.providers.microsoft import views as microsoft_views
from allauth.socialaccount.providers.oauth2.client import OAuth2Client
from dj_rest_auth.registration.views import SocialLoginView
from django.shortcuts import redirect
from django.urls import path, include
from rest_framework import routers
from django.contrib import admin

from rest_framework_simplejwt.authentication import JWTAuthentication
from rest_framework_simplejwt.views import TokenRefreshView
from rest_framework.views import APIView
from rest_framework.response import Response
from quickstart import views
from quickstart.views import SafeTokenRefreshView

router = routers.DefaultRouter()
router.register(r'users', views.UserViewSet)
router.register(r'groups', views.GroupViewSet)


class MicrosoftLogin(SocialLoginView):
    adapter_class = microsoft_views.MicrosoftGraphOAuth2Adapter
    client_class = OAuth2Client
    authentication_classes = [JWTAuthentication]


def microsoft_callback(request):
    params = urllib.parse.urlencode(request.GET)
    return redirect(f'http://localhost:3000/authRedirect?{params}')


class LogoutView(APIView):
    def get(self, request):
        response = Response({"message": "Logged out successfully"})
        response.delete_cookie('access_token')
        response.delete_cookie('refresh_token')
        return response


urlpatterns = [
    path('', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('server/auth/microsoft/', MicrosoftLogin.as_view()),
    path('server/auth/microsoft/login/callback/', microsoft_callback, name='microsoft_callback'),
    path('server/auth/microsoft/url/', microsoft_views.oauth2_login),
    path("admin/", admin.site.urls),
    path('api/token/refresh/', SafeTokenRefreshView.as_view(), name='token_refresh'),
    path('logout/', LogoutView.as_view(), name='logout'),
]
